# Repository of lecture slides

Collection of lecture resources:
- [ ] [Slides](https://gitlab.com/sastibe/lectures/-/blob/main/data_science_in_real_life/datascience_vorlesung.pdf) for the talk "A real-life Data Science Application", held online (via uploaded video) in 2020. Includes the [R Script](https://gitlab.com/sastibe/lectures/-/blob/main/data_science_in_real_life/ds_R.R) used in the talk.
- [ ] [Slides](https://gitlab.com/sastibe/lectures/-/blob/main/ml_ops/ml_ops.pdf) for the talk "Machine Learning Operations", held in April 2022.


# License

These lectures are licensed under CC-BY-4.0, see [License]().

# Author
[Sebastian Schweer](https://www.sastibe.de)

# Feedback & Contributing

While contributions are not necessary or encouraged, any feedback regarding the slides or the talk is greatly appreciated. Please contact via sebastian.schweer@sastibe.de.

# Acknowledgements

The content on these slides is heavily indebted to these ressources:

- https://medium.com/quantmetry/evaluate-your-mlops-maturity-651f7ff15c17
- https://cloud.google.com/architecture/mlops-continuous-delivery-and-automation-pipelines-in-machine-learning

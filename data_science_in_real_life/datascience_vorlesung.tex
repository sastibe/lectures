\documentclass[aspectratio=169]{beamer}

\usetheme{boxes}
\usefonttheme{structuresmallcapsserif}
\setbeamertemplate{navigation symbols}{}
\usecolortheme{default}
\setbeamersize{text margin left=60pt,text margin right=60pt}

\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, textcomp}
\usepackage{times}
\usepackage[T1]{fontenc}


% Setup TikZ

\usepackage{tikz}
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]


\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

% Author, Title, etc.

\title[Data Science Vorlesung] 
{%
  Data Science in Real Life
}

\author[Schweer]
{
  Dr.~Sebastian Schweer
}

\institute[1 und 1]
{  
 
  Head of Enterprise Advanced Analytics \\
  1\&1 Telecommunication SE,
  Montabaur \& Karlsruhe
  
}

\date[FZI, 2020]
     {Gastvorlesung im Rahmen der Veranstaltung \\
       ``Business Data Analytics: Application and Tools''\\
     KIT, Institut für Wirtschaftsinformatik und Marketing (IISM)}



% The main document

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}


\section{Introduction}


\begin{frame}{How did I get here?}
  \begin{itemize}
   \item PhD in Mathematics (Statistics) in Heidelberg, June 2015. Topic: \textit{Statistical Inference for Discrete-Valued Stochastic Processes}
   \item 2015 - 2017: Consultant for P3 automotive GmbH (Stuttgart), Analytics and Data Science Projects
   \item 2018: Data Scientist at 1\&1 Telecommunication SE
   \item Since August 2018: Head of Enterprise Advanced Analytics
   \end{itemize}
\end{frame}

\begin{frame}{What do I do?}
  \begin{itemize}
  \item ``Enterprise Advanced Analytics'' consists of 7 full-time employees with various backgrounds
    \item Part of ``Enterprise Business Intelligence'', responsible for the central data warehouse for reporting and measuring of enterprise performance
    \item Mission: Development and operation of productive analytical models aimed at improving the performance of 1\&1 Telecommunication SE
  \end{itemize}
    
\end{frame}

\section{An almost real-life Data Science Application}

\begin{frame}{Data - Decision - Delivery}
  (Almost) all data science projects can be structured into:
  \begin{itemize}
  \item[Data] What data is available to answer the question? How is it structured, what are its flaws and strengths? What training cases for answering the question are available?
  \item[Decision] What question should be answered? What are the possible answers, and which constraints exist?
  \item[Delivery] What type of output is needed? What action will be taken based on the output of the model?
  \end{itemize}
\end{frame}



\begin{frame}{Let's sell golden shoes!}
  Consider the following (entirely hypothetical) example:
  \begin{itemize}
  \item[Task] Improve the current marketing campaign selling Golden Shoes.
  \item[Situation] Last month, 50000 letters with Golden Flyers were sent. This lead to the sale of 25000 shoes. Problem: A Golden Flyer costs 10 €, so we are tasked with finding ``the right customers'' for the upcoming campaign.
   \end{itemize}
\end{frame}

\subsection{Data}

\begin{frame}{Golden Shoes - Data}
Based on the campaign of last month, our dataset shows 50000 cases.
  \begin{itemize}
    \item [Target] Did the customer buy the Golden Shoes? 
    \item [Is Male] Is the customer male or female?
    \item [Shoe size] What shoe size does the customer have?
    \item [Is Urban] Does the customer live in an urban area?
    \item [Age] How old is the customer?
    \item [Bundesland] In which German Bundesland does the customer live?
   \end{itemize}
\begin{figure}
   \includegraphics[scale = 0.4]{sample_data.png}
\end{figure}
  
\end{frame}

\subsection{Decision}

\begin{frame}{Golden Shoes - Decision}
  First model choice: Random Forest, due to categorical data with many levels and ease of interpretability.

  Split data into Training / Test / Validation (each comprising 33\%), train on Training and look at performance on Validation:
\begin{figure}
  \includegraphics[scale = 0.3]{roc_validation.png}
\end{figure}
\textit{Very} good performance, needs to be checked.
\end{frame}

\begin{frame}{Golden Shoes - Decision 2}
Check most important features detected by the model:
\begin{figure}
  \includegraphics[scale = 0.3]{varimpplot.png}
\end{figure}
Appearance of ``Bundesland'' so high on the list is questionable. Thus, look at distribution in data set.
\end{frame}

\begin{frame}{Golden Shoes - Decision 3}
\begin{figure}
  \includegraphics[scale = 0.3]{analysis.png}
\end{figure}
\begin{tabular}{cl}  
           \begin{tabular}{l}
               \parbox{0.8\linewidth}{%  change the parbox width as appropiate
                 Campaign overperformance in NRW: Golden Flyers included Golden Leaflet awarding free jerseys on sale of Golden Shoes (Jersey entirely hypothetical)
 }
           \end{tabular}
           &  \begin{tabular}{c}
           \includegraphics[scale = 0.06]{bvb.png}
           \end{tabular}
\end{tabular}

\end{frame}

\begin{frame}{Golden Shoes - Decision 4}
  Mitigation: Delete ``NRW'' customers, and impute the missing data by randomly selecting people from other Bundesländer and ascribing them to ``NRW''. Test varying metaparameters for Random Forest on validation data set:
\begin{figure}
  \includegraphics[scale = 0.3]{roc_comparison.png}
\end{figure}

Slight favourite \textbf{green}, with an impressive AUC. Translates to a 0.902 AUC on test data set, so little to no overfitting.

\end{frame}

\subsection{Delivery}

\begin{frame}{Golden Shoes - Delivery}
  Business Need for the model? ``Find the right customers'', i.e. ``ensure maximal probability of success when sending out the Golden Flyer''.

  Model allows for prediction: Plot proportion of customers contacted (based on affinity) vs. precision, i.e. $\frac{\mathrm{true  positive}}{\mathrm{true positive} + \mathrm{false positive}}$
\begin{figure}
  \includegraphics[scale = 0.3]{precision_vs_proportion.png}
\end{figure}
\end{frame}

\begin{frame}{Golden Shoes - Decision Revisited}
Is the ``green model'' still the right one, based on this metric? Comparison on the Validation data set:
\begin{figure}
  \includegraphics[scale = 0.3]{precision_vs_proportion_comp.png}
\end{figure}
Yes, it is (whew).
\end{frame}

\begin{frame}
  The model enables us to predict scenarios:

\begin{enumerate}
\item [Baseline] All customers, 50 \% success rate, CpO (Cost per Order) 20 €.
\item [Scenario 1] Top 50 \% customers, 77 \% success rate, CpO 12.93 €.
\item  [Scenario 2] Top 25 \% customers, 83 \% success rate, CpO 12.04 €.
\end{enumerate}
With these numbers, a solid business case can be calculated.

  
\end{frame}

\section{General Reflections}

\subsection{Data}
\begin{frame}{Data: Know your Sources}
 \textbf{Domain Knowledge}
    \begin{itemize}
      \item ``Deeper'' understanding input data: Sources, restrictions, quality
      \item Allows for quick sanity checks
      \item Improves model quality through better feature engineering
    \end{itemize}
 \mbox{}\\[0.2cm]
 \textbf{Every Data Point a Measurement}
    \begin{itemize}
      \item Each input underlies statistics
      \item Quality of values, reasons for NaNs, relevant inputs to models
      \item Special importance: measuring target values
      \item E.g. Predictive Maintenance: technical input, but what is an ``incident''?
    \end{itemize}
\end{frame}

\subsection{Decision}


\begin{frame}{Modeling: Further Observations}
  \textbf{Tuning Models: Survival of the fittest}
  \begin{itemize}
    \item AUC not the only comparative measure. E.g., if the output should reflect probabilities, the \textit{Brier Score} could be used.
    \item Use the Train/Test/Validation splits to ensure against overfitting
    \item Fun but little reward: Model performance seldom in focus.
    \item Usually 3-4 different model families in competition for each model scenario (logistic regression, svm, neural networks, ...)
  \end{itemize}
 \mbox{}\\[0.2cm]
  \textbf{Categorical Data with many levels: Challenging but Rewarding}
  \begin{itemize}
   \item Clustering of categorical data: not to fine and not too coarse.
   \item Possibility of feature engineering, often with good results.
   \end{itemize}
\end{frame}


\begin{frame}{How good is my AUC?}
  The (highly subjective) \textbf{Schweer scale} of model performance in terms of AUC for models involving data of human interactions:\\\mbox{} \\[1cm]
    
  \begin{tabular}[center]{l|l}
    \textbf{AUC Range} & \textbf{Judgement} \\
    \hline
    > 0.95 & Implausibly good. Most likely false.\\
    \hline
    < 0.95, $\geq$ 0.90 & Dangerously good. Very likely false.\\
    \hline
    < 0.90, $\geq$ 0.80 & Very good. \\
    \hline
    < 0.80, $\geq$ 0.70 & Good. \\
    \hline
    < 0.70, $\geq$ 0.60 & Capable. \\
    \hline
    < 0.60, $\geq$ 0.50 & Unsuitable.
   \end{tabular}
\end{frame}

\subsection{Delivery}

\begin{frame}{Delivery}
  \textbf{Translating Business Needs in Mathematics} 
  \begin{itemize}
  \item What actions will be taken based on the output of the model?
  \item How does the model improve the ``naive'' approach?
  \item Which metric should be optimized? In our example \textit{precision}; if the question was ``I could send Golden Flyers and Pink Flyers, who should get which one?'', the target would be \textit{accuracy}.
  \end{itemize}
  \mbox{}\\[0.2cm]
  \textbf{Ex Post Analysis of Model Performance}
  \begin{itemize}
  \item Check for model performance and possible improvements
  \item Replace predictive business case with true value added.
  \end{itemize}
\end{frame}

%\begin{frame}{Delivery - Software Development}
%  The resulting Data Science models run automatically on production environments. This necessitates software development guidelines:
%  \begin{itemize}
%  \item Python and SQL as programming languages
%  \item Unit testing for productive code, each version is tested before deployment
%  \item Collaboration via git, accompanied by appropriate modularization of the code base
%  \end{itemize}
%\end{frame}



\section{Conclusion}

\begin{frame}{Books in my bookshelf}
  I highly recommend these three books:
  \begin{itemize}
  \item \textit{The Elements of Statistical Learning} by Trevor Hastie, Robert Tibshirani and Jerome Friedman. I look into this book whenever I am faced with a new or unusual modeling situation.
  \item \textit{An Introduction to Statistical Learning} by Gareth James, Daniela Witten, Trevor Hastie and Robert Tibshirani. I look into this book whenever I can't find anything in ESL.
  \item \textit{The Art of Statistics} by David Spiegelhalter. I wish I could have read this book ten years earlier, it presents an outlook to statistics I thouroughly enjoy.
  \end{itemize}
\end{frame}

\begin{frame}{Thank You}
  \begin{itemize}
    \item If this talk inspired interest, 1\&1 is always looking for Data Science talent: During university studies, as a cooperation partner for Master's Theses, and as a full-time employer after graduation. Contact: sebastian.schweer@1und1.de
    \item Thank you all for your attention. It has been fun.
  \end{itemize}
\end{frame}

\end{document}



\documentclass[aspectratio=169]{beamer}

\usetheme{PaloAlto}
\usefonttheme{structuresmallcapsserif}
\setbeamertemplate{navigation symbols}{}
\usecolortheme{default}
\setbeamersize{text margin left=60pt,text margin right=60pt}

\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{lmodern, textcomp}
\usepackage{times}
\usepackage[T1]{fontenc}


% Setup TikZ

\usepackage{tikz}
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]


\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

% Author, Title, etc.

\title[ML OPS] 
{%
  Machine Learning Operations
}

\author[Sebastian Schweer]
{
  Dr.~Sebastian Schweer
}

\institute[1 und 1]
{  
 
  Head of Enterprise Advanced Analytics \\
  1\&1 Telecommunication SE,
  Montabaur \& Karlsruhe
  
}

\date[FZI, 2022]
     {Gastvorlesung im Rahmen der Veranstaltung \\
       ``Business Data Analytics: Application and Tools''\\
     KIT, Institut für Wirtschaftsinformatik und Marketing (IISM)\\ 23.04.2022}



% The main document

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\begin{frame}{QR Code for the slides}
  \begin{figure}
    \includegraphics[height = \textheight]{slides_qr.png}
  \end{figure}
\end{frame}


\section{Introduction}


\begin{frame}{How did I get here?}
  \begin{itemize}
   \item PhD in Mathematics (Statistics) in Heidelberg, June 2015. Topic: \textit{Statistical Inference for Discrete-Valued Stochastic Processes}
   \item 2015 - 2017: Consultant for P3 automotive GmbH (Stuttgart), Analytics and Data Science Projects
   \item 2018: Data Scientist at 1\&1 Telecommunication SE
   \item Since August 2018: Head of Enterprise Advanced Analytics
   \end{itemize}
\end{frame}

\begin{frame}{What do I do?}
  \begin{itemize}
  \item ``Enterprise Advanced Analytics'' consists of 6 full-time employees with various backgrounds
    \item Part of ``Enterprise Business Intelligence'', responsible for the central data warehouse for reporting and measuring of enterprise performance
    \item Mission: Development and operation of productive ML models aimed at improving the performance of 1\&1 Telecommunication SE
  \end{itemize}
    
\end{frame}

\section{Example of a ML Application}

\begin{frame}{Data - Decision - Delivery}
  (Almost) all data science projects can be structured into:
  \begin{itemize}
  \item[Data] What data is available to answer the question? How is it structured, what are its flaws and strengths? What training cases for answering the question are available?
  \item[Decision] What question should be answered? What are the possible answers, and which constraints exist?
  \item[Delivery] What type of output is needed? What action will be taken based on the output of the model?
  \end{itemize}
\end{frame}



\begin{frame}{Let's sell Golden Shoes!}
  Consider the following (entirely hypothetical) example:
  \begin{itemize}
  \item[Task] Improve the current marketing campaign selling Golden Shoes.
  \item[Situation] Last month, 50000 letters with Golden Flyers were sent. This lead to the sale of 25000 shoes. Problem: A Golden Flyer costs 10 €, so we are tasked with finding ``the right customers'' for the upcoming campaign.
   \end{itemize}
\end{frame}

\subsection{Data}

\begin{frame}{A Nice and Tidy Dataset}
Based on the campaign of last month, our dataset shows 50000 cases.
  \begin{itemize}
    \item [Target] Did the customer buy the Golden Shoes? 
    \item [Is Male] Is the customer male or female?
    \item [Shoe size] What shoe size does the customer have?
    \item [Is Urban] Does the customer live in an urban area?
    \item [Age] How old is the customer?
    \item [Bundesland] In which German Bundesland does the customer live?
   \end{itemize}
\begin{figure}
   \includegraphics[scale = 0.4]{../data_science_in_real_life/sample_data.png}
\end{figure}
  
\end{frame}

\subsection{Decision}

\begin{frame}{First Model: Random Forest}
  Split data into Training / Test / Validation, train model and check performance:
\begin{figure}
  \includegraphics[scale = 0.3]{../data_science_in_real_life/roc_validation.png}
\end{figure}
\textit{Very} good performance, needs to be checked.
\end{frame}

\begin{frame}{Feature Importance of Random Forest model}
  \begin{minipage}[c]{0.58\textwidth}
\begin{figure}
  \includegraphics[width = \textwidth]{../data_science_in_real_life/varimpplot.png}
\end{figure}
  \end{minipage}
  \begin{minipage}[c]{0.4\textwidth}
    \begin{tiny}
      Appearance of ``Bundesland'' so high on the list questionable \\ $\rightarrow$ exploratory analysis of data set.
    \end{tiny}
  \end{minipage}
\end{frame}

\begin{frame}{Exploratory Data Analysis}
\begin{figure}
  \includegraphics[scale = 0.3]{../data_science_in_real_life/analysis.png}
\end{figure}
\pause
\begin{tabular}{cl}  
           \begin{tabular}{l}
             \parbox{0.8\linewidth}{
               \footnotesize{
                 Campaign overperformance in NRW: Golden Flyers included Golden Leaflet awarding free jerseys on sale of Golden Shoes
 }}
           \end{tabular}
           &  \begin{tabular}{c}
           \includegraphics[width = 0.15\linewidth]{bvb_trikot_2020.jpg}
           \end{tabular}
\end{tabular}

\end{frame}

\begin{frame}{Feature Engineering / Mitigation}
  \footnotesize{
  Mitigation: Delete ``NRW'' customers, and impute the missing data by randomly selecting people from other Bundesländer and ascribing them to ``NRW''. Test varying metaparameters for Random Forest on validation data set:}
\begin{figure}
  \includegraphics[scale = 0.3]{../data_science_in_real_life/roc_comparison.png}
\end{figure}
\footnotesize{
Slight favourite \textbf{green}, with an impressive AUC. Translates to a 0.902 AUC on test data set, so little to no overfitting.
}
\end{frame}

\subsection{Delivery}

\begin{frame}{Business Case Definition}
\footnotesize{
  ``Find the right customers'', i.e. ``ensure maximal probability of success when sending out Golden Flyer''.

  Model allows for prediction: Plot proportion of customers contacted (based on affinity) vs. precision, i.e. $\frac{\mathrm{true  positive}}{\mathrm{true positive} + \mathrm{false positive}}$}
\begin{figure}
  \includegraphics[scale = 0.3]{../data_science_in_real_life/precision_vs_proportion.png}
\end{figure}
\end{frame}

\begin{frame}{Model Validation based on Business Case}
Is the ``green model'' still the right one, based on this metric? Comparison on the Validation data set:
\begin{figure}
  \includegraphics[scale = 0.3]{../data_science_in_real_life/precision_vs_proportion_comp.png}
\end{figure}
Yes, it is (whew).
\end{frame}

\begin{frame}{Business Use Prediction}
  The model enables us to predict scenarios:

\begin{enumerate}
\item [Baseline] All customers, 50 \% success rate, CpO (Cost per Order) 20 €.
\item [Scenario 1] Top 50 \% customers, 77 \% success rate, CpO 12.93 €.
\item  [Scenario 2] Top 25 \% customers, 83 \% success rate, CpO 12.04 €.
\end{enumerate}
With these numbers, a solid business case can be calculated.

  
\end{frame}


\begin{frame}{Recap of 2020 Talk}
  \begin{figure}
  \includegraphics[scale = 0.3]{ds_just_statistics.jpg}
\end{figure}
  
\end{frame}

\section{ML OPS}


\begin{frame}{Why should you care about ML Ops?}

Most real world applications of ``productive'' Data Science code look like this (Source: \cite{mlhid})   
\begin{figure}
  \includegraphics[width = \textwidth]{hidden_technical_debt_ml.png}
\end{figure}


\end{frame}

\begin{frame}{7 aspects of ML Operations}
  \begin{footnotesize}
$\left.
\begin{tabular}{p{.7\textwidth}}
\begin{itemize}
  \item Business Integration
\end{itemize}
\end{tabular}
\right.$
\pause
$\left.
\begin{tabular}{p{.7\textwidth}}
\begin{itemize}
  \item Data Pipeline
\end{itemize}
\end{tabular}
\right\}{Data}$
\pause
$\left.
\begin{tabular}{p{.7\textwidth}}
\begin{itemize}
  \item Experiments \& Modeling
  \item Retraining
  \item Decision Management
\end{itemize}
\end{tabular}
\right\}{Decision}$
\pause
$\left.
\begin{tabular}{p{.7\textwidth}}
\begin{itemize}
\item Deployment
\item Performance Measurement
\end{itemize}
\end{tabular}
\right\}{Delivery}$
\end{footnotesize}
\end{frame}

\begin{frame}{An Example from Uber: Michelangelo}
  \begin{figure}
    \includegraphics[width = \textwidth]{uber_michelangelo.png}
  \end{figure}
\end{frame}

\begin{frame}
  There are three levels of Maturity for each aspect:
  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item Proof of Concept
  \item Proof of Value
  \item Continuous Improvement
  \end{enumerate}
\end{frame}

  
\subsection{Business Integration}
\begin{frame}{Business Integration}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Singular new idea for the improvement of an existing process; Data Scientist level
  \item \textbf{Proof of Value:}\\ Singular implementations of ML processes that regularly improve specialized business KPIs; Department level
  \item \textbf{Continuous Improvement:}\\ Many (is not all) new processes are implemented with ML as a key factor; C-Level.
  \end{enumerate}
\end{frame}


\subsection{Data Pipeline}
\begin{frame}{Data Pipeline}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Manual process needed to prepare the data; sources of various types (Excel, logs, complex SQL queries, ...)
  \item \textbf{Proof of Value:}\\ Automatized process for data preparation but specialized to application; historization and versioning in place.
  \item \textbf{Continuous Improvement:}\\ Data is easily accessible across domains in ``Feature Stores'' and reproducible Data Pipelines; high effort is placed on obtaining new data sources.
  \end{enumerate}
\end{frame}


\subsection{Experiments and Modeling}
\begin{frame}{Experiments and Modeling}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Scripts are written in Jupyter Notebooks or similar; ML code runs on local computers or sandboxed VMs.
  \item \textbf{Proof of Value:}\\ Code is written in reproducible package in collaboration among data scientists; ML code runs on specialized hardware or cloud.
  \item \textbf{Continuous Improvement:}\\ Automated ML pipelines (data preprocessing to model output) with corresponding metadata store and model registry; infrastructure is scalable and adaptable to any new use case.
  \end{enumerate}
\end{frame}


\subsection{Retraining}
\begin{frame}{Retraining}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Models are seldom retrained, if at all; Only Data Scientists have the necessary skills
  \item \textbf{Proof of Value:}\\ Models are retrained on a regular basis; changes in data etc. requires Data Scientist to configure
  \item \textbf{Continuous Improvement:}\\ Retraining pipeline is automatic and can be triggered by decrease in model performance, new data etc., no Data Scientist needed
  \end{enumerate}
\end{frame}



\subsection{Decision Management}
\begin{frame}{Decision Management}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Human in the loop (HITL): ML provides assistance or insights into manual job 
  \item \textbf{Proof of Value:}\\ Human on the loop (HOTL): ML makes micro-decisions, but final decision is adjusted by human rules and parameters
  \item \textbf{Continuous Improvement:}\\ Human out of the loop (HOOTL): ML makes every decision, human only supplues constraints and objectives.
  \end{enumerate}
\end{frame}


\subsection{Deployment}
\begin{frame}{Deployment}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Manual process needed to plug model into the productive environment; Constraints on languages or architecture of the systems involved.
  \item \textbf{Proof of Value:}\\ DevOps: ML code is deployed via CI/CD pipelines with automated builds and testing (CI/CD) to avoid errors in the ML code
  \item \textbf{Continuous Improvement:}\\ MLOps: ML code is deployed via CI/CD pipelines with additional CT (continuous testing) stage to ensure up-to-date models 
  \end{enumerate}
\end{frame}


\subsection{Performance Measurement}
\begin{frame}{Performance Measurement}

  \begin{enumerate}
    \setcounter{enumi}{-1}
  \item \textbf{Proof of Concept:}\\ Ex post analysis of metrics; Metrics are chosen by Data Scientists and are highly specialized to the application
  \item \textbf{Proof of Value:}\\ Model performance is validated through A/B tests, but only monitored by a specialized audience; Metrics for model performance include business driven KPIs as well as operational KPIs (response times etc.)
  \item \textbf{Continuous Improvement:}\\ New models are automatically validated in A/B tests against the current default (Champion-Challenger) wrt analytical, business and operatilnal measurements; Validation performance in A/B tests is accessible to general audience
  \end{enumerate}
\end{frame}


\begin{frame}{Cheat Sheet}

  7 aspects of ML-OPS in 3 levels of maturity 
\renewcommand{\arraystretch}{2}
\begin{minipage}[t]{\linewidth}\hspace{-60pt}\vspace{20pt}
\begin{tiny}
\resizebox{1.4\linewidth}{!}{
  \begin{tabular}{p{0.10\linewidth} p{0.2\linewidth} p{0.2\linewidth} p{0.2\linewidth} p{0.2\linewidth} p{0.2\linewidth} p{0.2\linewidth} p{0.2\linewidth}}
  \mbox{}  & Business Integration & Data Pipeline & Experiments  &   Retraining & Decision Mgmt. & Deployment & Performance Measuremt. \\ \hline
    \textbf{PoC} & Ex-post for existing processes  & Manual, CSV/SQL & Jupyter Notebooks  & Never or Rarely, DS necessary & Human in the loop & Manual & Ad hoc analysis \\
    \textbf{PoV} & Improving singular existing processes & Automated but specialized  & Collaborative \& reproducible Python packages  & Regular intervals, DS necessary for changes & Human on the loop & DevOps & Customized A/B test, specialized audience \\
    \textbf{Cont. Improv.} & Improving possibly all new processes & Feature store &  ML Model Registry \& Metadata Store  & Automatic and/or actively triggered, no DS necessary & Human out of the loop & ML-Ops & Automated A/B test regime, general audience
\end{tabular}}
\end{tiny}
\end{minipage}
\end{frame}

\section{Concluding Thoughts}

\begin{frame}{Career options: Data Scientist or ML Engineer?}
  \begin{figure}
    \includegraphics[width = \textwidth]{google_trends_ds.png}
  \end{figure}
  \begin{itemize}
  \item Specialist view: Focus on ``Data Science only''. Good choice for mature organizations and environments.
  \item Generalist view: Take a broader view including several (if not all) aspects of ML Op
  \end{itemize}

\end{frame}

\begin{frame}{Rules I wish I'd known earlier}
\begin{figure}
  \includegraphics[width = 0.7\textwidth]{first_rule_of_ml.jpg}
\end{figure}
Loosely cited from \cite{googl2}
\end{frame}

\begin{frame}{Thank You}
  \begin{itemize}
  \item If this talk inspired interest, 1\&1 is always looking for Data Science talent: During university studies, as a cooperation partner for Master's Theses, and as a full-time employer after graduation. Contact: sebastian.schweer@1und1.de
    \item All slides (as well as those of 2020) are available here: https://gitlab.com/sastibe/lectures.git
    \item Thank you all for your attention. It has been fun.
  \end{itemize}
\end{frame}

\begin{frame}{Ressources}
  \footnotesize{
  \begin{thebibliography}{}
  \bibitem[Hidden Debt in ML Systems]{mlhid} Hidden Technical Debt in Machine Learning Systems, https://tinyurl.com/2p8p27et
  \bibitem[Google Level 1 Architecture]{googl1} https://cloud.google.com/architecture/mlops-continuous-delivery-and-automation-pipelines-in-machine-learning

  \bibitem[Google's Rules of ML]{googl2} https://developers.google.com/machine-learning/guides/rules-of-ml/
  \bibitem[7 Steps in ML-OPS]{mlops7steps} https://medium.com/quantmetry/evaluate-your-mlops-maturity-651f7ff15c17
  \end{thebibliography}}
\end{frame}

\end{document}
